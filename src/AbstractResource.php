<?php
/**
 * Resource
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab;

use GitLab\ClientInterface;

/**
 * Resource
 *
 * @since 1.0.0
 */
abstract class AbstractResource
{
    /**
     * GitLab HTTP client.
     *
     * @var GitLab\ClientInterface
     */
    protected $client = null;

    /**
     * Constructor.
     *
     * @since 1.0.0
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * Validates and filters the ID or URL-encoded path.
     *
     * @param $id ID or URL-encoded path of the project.
     * @return int|string
     */
    public final function getId($id)
    {
        if (is_numeric($id) && filter_var($id, FILTER_VALIDATE_INT, ['options' => ['min_range' => 1]]) === false) {
            throw new \InvalidArgumentException("ID number out of range.");
        }

        return filter_var($id, FILTER_SANITIZE_ENCODED);
    }
}
