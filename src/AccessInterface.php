<?php
/**
 * Access levels
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab;

/**
 * Valid access levels
 *
 * @link https://docs.gitlab.com/ee/api/access_requests.html
 * 
 * @since 1.0.0
 */
interface AccessInterface
{
    const NO_ACCESS = 0;

    const MINIMAL_ACCESS = 5;

    const GUEST = 10;

    const REPORTER = 20;

    const DEVELOPER = 30;

    const MAINTAINER = 40;

    const OWNER = 50;
}
