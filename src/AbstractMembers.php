<?php
/**
 * Group and project members API.
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab;

use Psr\Http\Message\ResponseInterface;

/**
 * Group and project members API.
 *
 * @link https://docs.gitlab.com/ee/api/members.html
 *
 * @since 1.0.0
 */
abstract class AbstractMembers extends AbstractResource
{
    /**
     * GitLab REST API context.
     *
     * @var string
     */
    private $context;

    /**
     * {@inheritDoc}
     *
     * @param ClientInterface $client GitLab HTTP client.
     * @param string          $context GitLab REST API context.
     */
    public function __construct(ClientInterface $client, string $context)
    {
        parent::__construct($client);

        if (in_array($context, ['groups', 'projects'])) {
            $this->context = $context;
        }
    }

    /**
     * Gets a list of group members viewable by the authenticated user.
     *
     * GET /groups/:id/members
     *
     * @link https://docs.gitlab.com/ee/api/members.html#list-all-members-of-a-group-or-project
     *
     * @since 1.0.0
     *
     * @param mixed  $id The ID or URL-encoded path of the group owned by the authenticated user.
     * @param string $query A query string to search for members.
     */
    public function getMembers($id, string $query = ''): ResponseInterface
    {
        $_id = $this->getId($id);

        $endpoint = sprintf('%s/%s/members', $this->context, $_id);

        return $this->client->request('GET', $endpoint, [
            'query' => !empty($query) ? ['query' => $query] : []
        ]);
    }

    /**
     * Gets a list of group members viewable by the authenticated user,
     * including inherited members.
     *
     * GET /groups/:id/members/all
     *
     * @link https://docs.gitlab.com/ee/api/members.html#list-all-members-of-a-group-or-project-including-inherited-members
     *
     * @since 1.0.0
     *
     * @param mixed  $id The ID or URL-encoded path of the group owned by the authenticated user.
     * @param string $query A query string to search for members.
     */
    public function getAllMembers($id, string $query = ''): ResponseInterface
    {
        $_id = $this->getId($id);

        $endpoint = sprintf('%s/%s/members/all', $this->context, $_id);

        return $this->client->request('GET', $endpoint, [
            'query' => !empty($query) ? ['query' => $query] : []
        ]);
    }

    /**
     * Get a member of a group.
     *
     * GET /groups/:id/members/:user_id
     *
     * @link https://docs.gitlab.com/ee/api/members.html#get-a-member-of-a-group-or-project
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the group owned by the authenticated user.
     * @param int   $user_id The user ID of the member.
     */
    public function getMember($id, int $user_id): ResponseInterface
    {
        $_id = $this->getId($id);

        $endpoint = sprintf('%s/%s/members/%s', $this->context, $_id, $user_id);

        return $this->client->request('GET', $endpoint);
    }
}
