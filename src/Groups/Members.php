<?php
/**
 * Members API
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab\Groups;

use GitLab\AbstractMembers;
use GitLab\ClientInterface;

/**
 * Members API
 *
 * @link https://docs.gitlab.com/ee/api/members.html
 *
 * @since 1.0.0
 */
final class Members extends AbstractMembers
{
    /**
     * {@inheritDoc}
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client, 'groups');
    }
}
