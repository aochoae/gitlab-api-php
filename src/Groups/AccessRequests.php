<?php
/**
 * Group access requests API
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab\Groups;

use GitLab\AbstractAccessRequests;
use GitLab\ClientInterface;

/**
 * Group access requests API
 *
 * @link https://docs.gitlab.com/ee/api/access_requests.html
 *
 * @since 1.0.0
 */
final class AccessRequests extends AbstractAccessRequests
{
    /**
     * {@inheritDoc}
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client, 'groups');
    }
}
