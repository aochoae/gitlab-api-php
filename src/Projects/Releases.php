<?php
/**
 * Releases API
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab\Projects;

use GitLab\AbstractResource;
use GitLab\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Releases API
 *
 * @link https://docs.gitlab.com/ee/api/releases/index.html
 *
 * @since 1.0.0
 */
final class Releases extends AbstractResource
{
    /**
     * {@inheritDoc}
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
    }

    /**
     * Paginated list of Releases.
     *
     * GET /projects/:id/releases
     *
     * @link https://docs.gitlab.com/ee/api/releases/index.html#list-releases
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project.
     */
    public function getReleases($id): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('GET', "projects/$project_id/releases");
    }

    /**
     * Get a Release for the given tag.
     *
     * GET /projects/:id/releases/:tag_name
     *
     * @link https://docs.gitlab.com/ee/api/releases/index.html#get-a-release-by-a-tag-name
     *
     * @since 1.0.0
     *
     * @param mixed  $id The ID or URL-encoded path of the project.
     * @param string $tag_name The tag where the release will be created from.
     */
    public function getRelease($id, string $tag_name): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('GET', "projects/$project_id/releases/$tag_name");
    }

    /**
     * Create a release.
     *
     * POST /projects/:id/releases
     *
     * @link https://docs.gitlab.com/ee/api/releases/index.html#create-a-release
     *
     * @since 1.0.0
     *
     * @param mixed  $id The ID or URL-encoded path of the project.
     * @param string $name The release name.
     * @param string $tag_name The tag where the release will be created from.
     * @param string $description The description of the release.
     * @param array  $attributes Parameters.
     */
    public function create($id, string $name, string $tag_name, string $description, array $attributes = []): ResponseInterface
    {
        $project_id = $this->getId($id);

        $mandatory = compact("name", "tag_name", "description");

        $query = $mandatory + $attributes;

        return $this->client->request('POST', "projects/$project_id/releases", [
            'query' => $query
        ]);
    }

    /**
     * Update a release
     *
     * PUT /projects/:id/releases/:tag_name
     *
     * @link https://docs.gitlab.com/ee/api/releases/index.html#update-a-release
     *
     * @since 1.0.0
     *
     * @param mixed  $id The ID or URL-encoded path of the project.
     * @param string $tag_name The tag where the release will be created from.
     * @param array  $attributes Parameters.
     */
    public function update($id, string $tag_name, array $attributes = []): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('PUT', "projects/$project_id/releases/$tag_name", [
            'query' => $attributes
        ]);
    }

    /**
     * Delete a release
     *
     * DELETE /projects/:id/releases/:tag_name
     *
     * @link https://docs.gitlab.com/ee/api/releases/index.html#delete-a-release
     *
     * @since 1.0.0
     *
     * @param mixed  $id The ID or URL-encoded path of the project.
     * @param string $tag_name The tag where the release will be created from.
     */
    public function delete($id, string $tag_name): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('DELETE', "projects/$project_id/releases/$tag_name");
    }
}
