<?php
/**
 * Repository Files API
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab\Projects;

use GitLab\AbstractResource;
use GitLab\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Repository Files API
 *
 * @link https://docs.gitlab.com/ee/api/repository_files.html
 *
 * @since 1.0.0
 */
final class RepositoryFiles extends AbstractResource
{
    /**
     * {@inheritDoc}
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
    }

    /**
     * Allows you to receive information about file in repository like name,
     * size, content.
     *
     * GET /projects/:id/repository/files/:file_path
     * GET /projects/:id/repository/files/:file_path/raw
     *
     * @link https://docs.gitlab.com/ee/api/repository_files.html#get-file-from-repository
     * @link https://docs.gitlab.com/ee/api/repository_files.html#get-raw-file-from-repository
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project.
     * @param string $file_path The Url encoded full path to file.
     * @param string $ref The name of branch, tag or commit.
     * @param bool $raw Get raw file from repository.
     */
    public function getFile($id, string $file_path, string $ref = 'main', bool $raw = false): ResponseInterface
    {
        $project_id = $this->getId($id);

        $endpoint = sprintf( 'projects/%s/repository/files/%s%s', $project_id, $file_path, $raw ? '/raw' : '' );

        return $this->client->request('GET', $endpoint, [
            'query' => ['ref' => $ref]
        ]);
    }

    /**
     * Create new file in repository.
     *
     * POST /projects/:id/repository/files/:file_path
     *
     * @link https://docs.gitlab.com/ee/api/repository_files.html#create-new-file-in-repository
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project.
     * @param string $file_path The Url encoded full path to file.
     * @param string $content The file content.
     * @param string $branch The name of the branch.
     * @param array $attributes Parameters.
     */
    public function create($id, string $file_path, string $content, string $branch = 'main', array $attributes = []): ResponseInterface
    {
        $project_id = $this->getId($id);

        $mandatory = compact("content", "branch");

        $query = $mandatory + $attributes;

        return $this->client->request('POST', "projects/$project_id/repository/files/$file_path", [
            'query' => $query
        ]);
    }

    /**
     * Update existing file in repository.
     *
     * PUT /projects/:id/repository/files/:file_path
     *
     * @link https://docs.gitlab.com/ee/api/repository_files.html#update-existing-file-in-repository
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project.
     * @param string $file_path The Url encoded full path to file.
     * @param string $content The file content.
     * @param string $commit_message The commit message.
     * @param string $branch The name of the branch.
     * @param array $attributes Parameters.
     */
    public function update($id, string $file_path, string $content, string $commit_message, string $branch = 'main', array $attributes = []): ResponseInterface
    {
        $project_id = $this->getId($id);

        $mandatory = compact("content", "branch", "commit_message");

        $query = $mandatory + $attributes;

        return $this->client->request('PUT', "projects/$project_id/repository/files/$file_path", [
            'query' => $query
        ]);
    }

    /**
     * Delete existing file in repository.
     *
     * DELETE /projects/:id/repository/files/:file_path
     *
     * @link https://docs.gitlab.com/ee/api/repository_files.html#delete-existing-file-in-repository
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project.
     * @param string $file_path The Url encoded full path to file.
     * @param string $commit_message The commit message.
     * @param string $branch The name of the branch.
     * @param array $attributes Parameters.
     */
    public function delete($id, string $file_path, string $commit_message, string $branch = 'main', array $attributes = []): ResponseInterface
    {
        $project_id = $this->getId($id);

        $mandatory = compact("branch", "commit_message");

        $query = $mandatory + $attributes;

        return $this->client->request('DELETE', "projects/$project_id/repository/files/$file_path", [
            'query' => $query
        ]);
    }
}
