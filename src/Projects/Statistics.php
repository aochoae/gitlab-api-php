<?php
/**
 * Project statistics API
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab\Projects;

use GitLab\AbstractResource;
use GitLab\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Project statistics API
 *
 * @link https://docs.gitlab.com/ee/api/project_statistics.html
 *
 * @since 1.0.0
 */
final class Statistics extends AbstractResource
{
    /**
     * {@inheritDoc}
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
    }

    /**
     * Get the statistics of the last 30 days
     *
     * GET /projects/:id/statistics
     *
     * @link https://docs.gitlab.com/ee/api/project_statistics.html
     *
     * @param mixed $id The ID or URL-encoded path of the project.
     */
    public function getStatistics($id): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('GET', "projects/$project_id/statistics");
    }
}

