<?php
/**
 * Project snippets API
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab\Projects;

use GitLab\AbstractResource;
use GitLab\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Project snippets API
 *
 * @link https://docs.gitlab.com/ee/api/project_snippets.html
 *
 * @since 1.0.0
 */
final class Snippets extends AbstractResource
{
    /**
     * {@inheritDoc}
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
    }

    /**
     * Get a list of project snippets.
     *
     * GET /projects/:id/snippets
     *
     * @link https://docs.gitlab.com/ee/api/project_snippets.html#list-snippets
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project.
     */
    public function getSnippets($id): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('GET', "projects/$project_id/snippets");
    }

    /**
     * Get a single project snippet.
     *
     * GET /projects/:id/snippets/:snippet_id
     *
     * @link https://docs.gitlab.com/ee/api/project_snippets.html#single-snippet
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project.
     * @param int $snippet_id The ID of a project's snippet.
     */
    public function getSnippet($id, int $snippet_id): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('GET', "projects/$project_id/snippets/$snippet_id");
    }

    /**
     * Creates a new project snippet.
     *
     * POST /projects/:id/snippets
     *
     * @link https://docs.gitlab.com/ee/api/project_snippets.html#create-new-snippet
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project owned by the
     *      authenticated user.
     * @param string $title The title of the snippet.
     * @param string $file_name The file name of the snippet file.
     * @param string $code The content of the snippet.
     * @param string $visibility The snippet's visibility.
     * @param string $description The description of the snippet.
     */
    public function create($id, string $title, string $file_name, string $code, string $visibility, string $description = ''): ResponseInterface
    {
        $project_id = $this->getId($id);

        $query = compact("title", "file_name", "code", "visibility", "description");

        return $this->client->request('POST', "projects/$project_id/snippets", [
            'query' => $query
        ]);
    }

    /**
     * Updates an existing project snippet.
     *
     * PUT /projects/:id/snippets/:snippet_id
     *
     * @link https://docs.gitlab.com/ee/api/project_snippets.html#update-snippet
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project owned by the
     *      authenticated user.
     * @param int $snippet_id The ID of a project's snippet.
     * @param array $attributes Parameters.
     */
    public function update($id, int $snippet_id, array $attributes = []): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('PUT', "projects/$project_id/snippets/$snippet_id", [
            'query' => $attributes
        ]);
    }

    /**
     * Deletes an existing project snippet.
     *
     * DELETE /projects/:id/snippets/:snippet_id
     *
     * @link https://docs.gitlab.com/ee/api/project_snippets.html#delete-snippet
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project owned by the
     *      authenticated user.
     * @param int $snippet_id The ID of a project's snippet.
     */
    public function delete($id, int $snippet_id): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('DELETE', "projects/$project_id/snippets/$snippet_id");
    }

    /**
     * Returns the raw project snippet as plain text.
     *
     * GET /projects/:id/snippets/:snippet_id/raw
     *
     * @link https://docs.gitlab.com/ee/api/project_snippets.html#snippet-content
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project.
     * @param int $snippet_id The ID of a project’s snippet.
     */
    public function getContent($id, int $snippet_id): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('GET', "projects/$project_id/snippets/$snippet_id/raw");
    }

    /**
     * Get user agent details.
     *
     * GET /projects/:id/snippets/:snippet_id/user_agent_detail
     *
     * @link https://docs.gitlab.com/ee/api/project_snippets.html#get-user-agent-details
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project.
     * @param int $snippet_id The ID of a project’s snippet.
     */
    public function getUserAgent($id, int $snippet_id): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('GET', "projects/$project_id/snippets/$snippet_id/user_agent_detail");
    }
}
