<?php
/**
 * Protected Branches API
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab\Projects;

use GitLab\AbstractResource;
use GitLab\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Protected Branches API
 *
 * @link https://docs.gitlab.com/ee/api/branches.html
 *
 * @since 1.0.0
 */
final class ProtectedBranches extends AbstractResource
{
    /**
     * {@inheritDoc}
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
    }

    /**
     * Gets a list of protected branches from a project.
     *
     * GET /projects/:id/protected_branches
     *
     * @link https://docs.gitlab.com/ee/api/protected_branches.html#list-protected-branches
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project owned by the
     *      authenticated user.
     */
    public function getBranches($id): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('GET', "projects/$project_id/protected_branches");
    }
}
