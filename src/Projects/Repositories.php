<?php
/**
 * Repositories API
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab\Projects;

use GitLab\AbstractResource;
use GitLab\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Repositories API
 *
 * @link https://docs.gitlab.com/ee/api/repositories.html
 *
 * @since 1.0.0
 */
final class Repositories extends AbstractResource
{
    /**
     * {@inheritDoc}
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
    }

    /**
     * Get a list of repository files and directories in a project.
     *
     * GET /projects/:id/repository/tree
     *
     * @link https://docs.gitlab.com/ee/api/repositories.html#list-repository-tree
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project owned by the
     *      authenticated user.
     * @param array $attributes Parameters.
     */
    public function getTree($id, array $attributes = []): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('GET', "projects/$project_id/repository/tree", [
            'query' => $attributes
        ]);
    }

    /**
     * Allows you to receive information about blob in repository like size and
     * content.
     *
     * GET /projects/:id/repository/blobs/:sha
     * GET /projects/:id/repository/blobs/:sha/raw
     *
     * @link https://docs.gitlab.com/ee/api/repositories.html#get-a-blob-from-repository
     * @link https://docs.gitlab.com/ee/api/repositories.html#raw-blob-content
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project owned by the
     *      authenticated user.
     * @param string $sha The blob SHA.
     * @param bool $raw Allows to get the raw file contents for a blob by blob SHA.
     */
    public function getBlob($id, string $sha, bool $raw = false): ResponseInterface
    {
        $project_id = $this->getId($id);

        $endpoint = sprintf("projects/%s/repository/blobs/%s%s", $project_id, $sha, $raw ? '/raw' : '');

        return $this->client->request('GET', $endpoint);
    }

    /**
     * Get an archive of the repository.
     *
     * GET /projects/:id/repository/archive[.format]
     *
     * @link https://docs.gitlab.com/ee/api/repositories.html#get-file-archive
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project owned by the
     *      authenticated user.
     * @param string $format The suffix for the archive format.
     * @param string $sha The blob SHA.
     */
    public function getFile($id, string $format = 'tar.gz', string $sha = ''): ResponseInterface
    {
        $project_id = $this->getId($id);

        $formats = [
            'tar.gz',
            'tar.bz2',
            'tbz',
            'tbz2',
            'tb2',
            'bz2',
            'tar',
            'zip'
        ];

        $suffix = in_array($format, $formats) ? ".$format" : '';

        return $this->client->request('GET', "projects/$project_id/repository/archive$suffix", [
            'query' => !empty($sha) ? ['sha' => $sha] : []
        ]);
    }

    /**
     * Get repository contributors list.
     *
     * GET /projects/:id/repository/contributors
     *
     * @link https://docs.gitlab.com/ee/api/repositories.html#contributors
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project owned by the
     *      authenticated user.
     * @param array $attributes Parameters.
     */
    public function getContributors($id, array $attributes = []): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('GET', "projects/$project_id/repository/contributors", [
            'query' => $attributes
        ]);
    }

    /**
     * Compare branches, tags or commits.
     *
     * GET /projects/:id/repository/compare
     *
     * @link https://docs.gitlab.com/ee/api/repositories.html#compare-branches-tags-or-commits
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project owned by the
     *      authenticated user.
     * @param string $from The commit SHA or branch name.
     * @param string $to The commit SHA or branch name.
     * @param array $attributes Parameters.
     */
    public function compare($id, string $from, string $to, array $attributes = []): ResponseInterface
    {
        $project_id = $this->getId($id);

        $mandatory = compact("from", "to");

        $query = $mandatory + $attributes;

        return $this->client->request('GET', "projects/$project_id/repository/compare", [
            'query' => $query
        ]);
    }
}
