<?php
/**
 * Branches API
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab\Projects;

use GitLab\AbstractResource;
use GitLab\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Branches API
 *
 * @link https://docs.gitlab.com/ee/api/branches.html
 *
 * @since 1.0.0
 */
final class Branches extends AbstractResource
{
    /**
     * {@inheritDoc}
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
    }

    /**
     * Get a list of repository branches from a project, sorted by name
     * alphabetically.
     *
     * GET /projects/:id/repository/branches
     *
     * @link https://docs.gitlab.com/ee/api/branches.html#list-repository-branches
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project owned by the
     *      authenticated user.
     * @param string $search Search for specific branches containing the
     *      search string.
     */
    public function getBranches($id, string $search = ''): ResponseInterface
    {
        $project_id = $this->getId($id);

        $query = filter_var($search, FILTER_SANITIZE_ENCODED);

        return $this->client->request('GET', "projects/$project_id/repository/branches", [
            'query' => !empty($query) ? ['search' => $query] : []
        ]);
    }

    /**
     * Get a single project repository branch.
     *
     * GET /projects/:id/repository/branches/:branch
     *
     * @link https://docs.gitlab.com/ee/api/branches.html#get-single-repository-branch
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project owned by the
     *      authenticated user.
     * @param string $branch Name of the branch.
     */
    public function getBranch($id, string $branch): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('GET', "projects/$project_id/repository/branches/$branch");
    }

    /**
     * Create a new branch in the repository.
     *
     * POST /projects/:id/repository/branches
     *
     * @link https://docs.gitlab.com/ee/api/branches.html#create-repository-branch
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project owned by the
     *      authenticated user.
     * @param string $branch Name of the branch.
     * @param string $ref Branch name or commit SHA to create branch from.
     */
    public function create($id, string $branch, string $ref): ResponseInterface
    {
        $project_id = $this->getId($id);

        $query = compact("branch", "ref");

        return $this->client->request('POST', "projects/$project_id/repository/branches", [
            'query' => $query
        ]);
    }

    /**
     * Delete a branch from the repository.
     *
     * DELETE /projects/:id/repository/branches/:branch
     *
     * @link https://docs.gitlab.com/ee/api/branches.html#delete-repository-branch
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project owned by the
     *      authenticated user.
     * @param string $branch Name of the branch.
     */
    public function delete($id, string $branch): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('DELETE', "projects/$project_id/repository/branches/$branch");
    }

    /**
     * Delete all branches that are merged into the project’s default branch.
     *
     * DELETE /projects/:id/repository/merged_branches
     *
     * @link https://docs.gitlab.com/ee/api/branches.html#delete-merged-branches
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project owned by the
     *      authenticated user.
     */
    public function deleteMergedBranches($id): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('DELETE', "projects/$project_id/repository/merged_branches");
    }
}
