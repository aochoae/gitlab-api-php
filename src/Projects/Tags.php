<?php
/**
 * Tags API
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab\Projects;

use GitLab\AbstractResource;
use GitLab\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Tags API
 *
 * @link https://docs.gitlab.com/ee/api/tags.html
 *
 * @since 1.0.0
 */
final class Tags extends AbstractResource
{
    /**
     * {@inheritDoc}
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
    }

    /**
     * Get a list of repository tags from a project.
     *
     * GET /projects/:id/repository/tags
     *
     * @link https://docs.gitlab.com/ee/api/tags.html#list-project-repository-tags
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project owned by the
     *      authenticated user.
     * @param array $attributes Parameters.
     */
    public function getTags($id, array $attributes = []): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('GET', "projects/$project_id/repository/tags", [
            'query' => $attributes
        ]);
    }

    /**
     * Get a specific repository tag determined by its name.
     *
     * GET /projects/:id/repository/tags/:tag_name
     *
     * @link https://docs.gitlab.com/ee/api/tags.html#get-a-single-repository-tag
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project owned by the
     *      authenticated user.
     * @param string $tag_name The name of the tag.
     */
    public function getTag($id, string $tag_name): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('GET', "projects/$project_id/repository/tags/$tag_name");
    }

    /**
     * Create a new tag.
     *
     * POST /projects/:id/repository/tags
     *
     * @link https://docs.gitlab.com/ee/api/tags.html#create-a-new-tag
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project owned by the
     *      authenticated user.
     * @param string $tag_name The name of the tag.
     * @param string $ref Create tag using commit SHA, another tag name, or
     *      branch name.
     * @param array $attributes Parameters.
     */
    public function create($id, string $tag_name, string $ref, array $attributes = []): ResponseInterface
    {
        $project_id = $this->getId($id);

        $mandatory = compact("tag_name", "ref");

        $query = $mandatory + $attributes;

        return $this->client->request('POST', "projects/$project_id/repository/tags", [
            'query' => $query
        ]);
    }

    /**
     * Deletes a tag of a repository with given name.
     *
     * DELETE /projects/:id/repository/tags/:tag_name
     *
     * @link https://docs.gitlab.com/ee/api/tags.html#delete-a-tag
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project owned by the
     *      authenticated user.
     * @param string $tag_name The name of the tag.
     */
    public function delete($id, string $tag_name): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('DELETE', "projects/$project_id/repository/tags/$tag_name");
    }
}
