<?php
/**
 * Projects API
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab\Projects;

use GitLab\AbstractResource;
use GitLab\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Projects API
 *
 * @link https://docs.gitlab.com/ee/api/projects.html
 *
 * @since 1.0.0
 */
final class Projects extends AbstractResource
{
    /**
     * {@inheritDoc}
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
    }

    /**
     * Get a list of all visible projects across GitLab for the authenticated
     * user.
     *
     * GET /projects
     *
     * @link https://docs.gitlab.com/ee/api/projects.html#list-all-projects
     *
     * @since 1.0.0
     *
     * @param array $attributes Parameters.
     */
    public function getProjects(array $attributes = []): ResponseInterface
    {
        return $this->client->request('GET', 'projects', [
            'query' => $attributes
        ]);
    }

    /**
     * Get a list of visible projects owned by the given user.
     *
     * GET /users/:user_id/projects
     *
     * @link https://docs.gitlab.com/ee/api/projects.html#list-user-projects
     *
     * @since 1.0.0
     *
     * @param mixed $user_id The ID or username of the user.
     * @param array $attributes Parameters.
     */
    public function getUserProjects($user_id, array $attributes = []): ResponseInterface
    {
        $user_id = $this->getId($user_id);

        return $this->client->request('GET', "users/$user_id/projects", [
            'query' => $attributes
        ]);
    }

    /**
     * Get a specific project.
     *
     * GET /projects/:id
     *
     * @link https://docs.gitlab.com/ee/api/projects.html#get-single-project
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project.
     * @param array $attributes Parameters.
     */
    public function getProject($id, array $attributes = []): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('GET', "projects/$project_id", [
            'query' => $attributes
        ]);
    }

    /**
     * Get the users list of a project.
     *
     * GET /projects/:id/users
     *
     * @link https://docs.gitlab.com/ee/api/projects.html#get-project-users
     *
     * @since 1.0.0
     *
     * @param mixed  $id The ID or URL-encoded path of the project.
     * @param string $search Search for specific users.
     */
    public function getProjectUsers($id, string $search = ''): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('GET', "projects/$project_id/users", [
            'query' => !empty($search) ? ['search' => $search] : []
        ]);
    }

    /**
     * Creates a new project owned by the authenticated user.
     *
     * POST /projects
     *
     * @link https://docs.gitlab.com/ee/api/projects.html#create-project
     *
     * @since 1.0.0
     *
     * @param array $attributes Parameters.
     */
    public function create(array $attributes): ResponseInterface
    {
        return $this->client->request('POST', "projects", [
            'query' => $attributes
        ]);
    }

    /**
     * Creates a new project owned by the specified user. Available only for
     * admins.
     *
     * POST /projects/user/:user_id
     *
     * @link https://docs.gitlab.com/ee/api/projects.html#create-project-for-user
     *
     * @since 1.0.0
     *
     * @param int    $user_id The user ID of the project owner.
     * @param string $name The name of the new project.
     * @param array  $attributes Parameters.
     */
    public function createForUser(int $user_id, string $name, array $attributes = []): ResponseInterface
    {
        $attributes = array_merge($attributes, ['name' => $name]);

        return $this->client->request('POST', "projects/user/$user_id", [
            'query' => $attributes
        ]);
    }

    /**
     * Updates an existing project.
     *
     * PUT /projects/:id
     *
     * @link https://docs.gitlab.com/ee/api/projects.html#edit-project
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project.
     * @param array $attributes Parameters.
     */
    public function edit($id, array $attributes): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('PUT', "projects/$project_id", [
            'query' => $attributes
        ]);
    }

    /**
     * Forks a project into the user namespace of the authenticated user or the one provided.
     *
     * POST /projects/:id/fork
     *
     * @link https://docs.gitlab.com/ee/api/projects.html#fork-project
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project.
     * @param mixed $namespace The ID or path of the namespace that the project
     *      will be forked to.
     * @param array $attributes Parameters.
     */
    public function fork($id, $namespace, array $attributes = []): ResponseInterface
    {
        $project_id = $this->getId($id);

        $query = array_merge( $attributes, [ 'namespace', $namespace ] );

        return $this->client->request('POST', "projects/$project_id/fork", [
            'query' => $query
        ]);
    }

    /**
     * List the projects accessible to the calling user that have an
     * established, forked relationship with the specified project.
     *
     * GET /projects/:id/forks
     *
     * @link https://docs.gitlab.com/ee/api/projects.html#list-forks-of-a-project
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project.
     * @param array $attributes Parameters.
     */
    public function getForks($id, array $attributes = []): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('POST', "projects/$project_id/forks", [
            'query' => $attributes
        ]);
    }

    /**
     * Stars a given project.
     *
     * POST /projects/:id/star
     *
     * @link https://docs.gitlab.com/ee/api/projects.html#star-a-project
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project.
     */
    public function star($id): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('POST', "projects/$project_id/star");
    }

    /**
     * Unstars a given project.
     *
     * POST /projects/:id/unstar
     *
     * @link https://docs.gitlab.com/ee/api/projects.html#unstar-a-project
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project.
     */
    public function unstar($id): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('POST', "projects/$project_id/unstar");
    }

    /**
     * Get languages used in a project with percentage value.
     *
     * GET /projects/:id/languages
     *
     * @link https://docs.gitlab.com/ee/api/projects.html#languages
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project.
     */
    public function getLanguages($id): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('GET', "projects/$project_id/languages");
    }

    /**
     * Archives the project if the user is either admin or the project owner of
     * this project.
     *
     * POST /projects/:id/archive
     *
     * @link https://docs.gitlab.com/ee/api/projects.html#archive-a-project
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project.
     */
    public function archive($id): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('POST', "projects/$project_id/archive");
    }

    /**
     * Unarchives the project if the user is either admin or the project owner
     * of this project.
     *
     * POST /projects/:id/unarchive
     *
     * @link https://docs.gitlab.com/ee/api/projects.html#unarchive-a-project
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project.
     */
    public function unarchive($id): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('POST', "projects/$project_id/unarchive");
    }

    /**
     * Removes a project including all associated resources.
     *
     * DELETE /projects/:id
     *
     * @link https://docs.gitlab.com/ee/api/projects.html#remove-project
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project.
     */
    public function delete($id): ResponseInterface
    {
        $project_id = $this->getId($id);

        return $this->client->request('DELETE', "projects/$project_id");
    }
}
