<?php
/**
 * Dockerfiles API
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab\Templates;

use GitLab\AbstractResource;
use GitLab\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Dockerfiles API
 *
 * @link https://docs.gitlab.com/ee/api/templates/dockerfiles.html
 *
 * @since 1.0.0
 */
final class Dockerfiles extends AbstractResource
{
    /**
     * {@inheritDoc}
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
    }

    /**
     * Get all Dockerfile templates.
     *
     * GET /templates/dockerfiles
     *
     * @link https://docs.gitlab.com/ee/api/templates/dockerfiles.html#list-dockerfile-templates
     *
     * @since 1.0.0
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getListTemplates(): ResponseInterface
    {
        return $this->client->request('GET', 'templates/dockerfiles');
    }

    /**
     * Get a single Dockerfile template.
     *
     * GET /templates/dockerfiles/:key
     *
     * @link https://docs.gitlab.com/ee/api/templates/dockerfiles.html#single-dockerfile-template
     *
     * @since 1.0.0
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getSingleTemplate(string $key): ResponseInterface
    {
        return $this->client->request('GET', "templates/dockerfiles/$key");
    }
}
