<?php
/**
 * GitLab Client
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab;

use GuzzleHttp\Client as GuzzleClient;
use Psr\Http\Message\ResponseInterface;

/**
 * Client used to interact with GitLab.
 *
 * @since 1.0.0
 */
final class Client implements ClientInterface
{
    /**
     * @constant GITLAB_API_URI
     */
    const GITLAB_API_URI = 'https://gitlab.com/api/v4/';

    /**
     * GuzzleHttp HTTP client.
     *
     * @var GuzzleClient HTTP client.
     */
    private $client;

    /**
     * The client's constructor.
     *
     * @since 1.0.0
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        // Configuration
        $config['base_uri'] = $config['base_uri'] ?? Client::GITLAB_API_URI;
        $config['http_errors'] = $config['http_errors'] ?? true;

        // GitLab client instance
        $this->client = new GuzzleClient($config);
    }

    /**
     * Send an HTTP request.
     *
     * @since 1.0.0
     */
    public function request($method, $uri, array $options = []): ResponseInterface
    {
        return $this->client->request($method, $uri, $options);
    }

    /**
     * {@inheritDoc}
     *
     * @since 1.0.0
     */
    public function getBaseUrl(): string
    {
        return (string) $this->client->getConfig('base_uri');
    }
}
