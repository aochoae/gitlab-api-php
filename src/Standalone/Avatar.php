<?php
/**
 * Avatar API
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab\Standalone;

use GitLab\AbstractResource;
use GitLab\ClientInterface;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;

/**
 * Avatar API
 *
 * @link https://docs.gitlab.com/ee/api/avatar.html
 *
 * @since 1.0.0
 */
final class Avatar extends AbstractResource
{
    /**
     * {@inheritDoc}
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
    }

    /**
     * Get a single avatar URL.
     *
     * GET /avatar
     *
     * @since 1.0.0
     *
     * @param string $email Public email address of the user.
     * @param int $size Single pixel dimension (since images are squares).
     */
    public function getAvatar(string $email, int $size = 48): ResponseInterface
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            throw new InvalidArgumentException("Invalid email.");
        }

        if (filter_var($size, FILTER_VALIDATE_INT, ['options' => ['min_range' => 1]]) === false) {
            throw new InvalidArgumentException("Size out of range.");
        }

        $query = compact("email", "size");

        return $this->client->request('GET', 'avatar', [
            'query' => $query
        ]);
    }
}
