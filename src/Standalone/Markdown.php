<?php
/**
 * Markdown API
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab\Standalone;

use GitLab\AbstractResource;
use GitLab\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Markdown API
 *
 * @link https://docs.gitlab.com/ee/api/markdown.html
 *
 * @since 1.0.0
 */
final class Markdown extends AbstractResource
{
    /**
     * {@inheritDoc}
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
    }

    /**
     * Render an arbitrary Markdown document.
     *
     * POST /markdown
     *
     * @link https://docs.gitlab.com/ee/api/markdown.html#render-an-arbitrary-markdown-document
     *
     * @since 1.0.0
     *
     * @param string $text The markdown text to render.
     * @param bool $gfm Render text using GitLab Flavored Markdown.
     * @param string $project Use project as a context when creating references
     *      using GitLab Flavored Markdown.
     */
    public function render(string $text, bool $gfm = false, string $project = ''): ResponseInterface
    {
        $multipart['multipart'] = [
            [
                'name'     => 'text',
                'contents' => $text,
                'headers'  => [
                    'Content-Type' => 'application/json'
                ]
            ],
            [
                'name'     => 'gfm',
                'contents' => $gfm,
                'headers'  => [
                    'Content-Type' => 'application/json'
                ]
            ]
        ];

        if (!empty($project)) {

            /*  Authentication is required if a project is not public */
            $multipart['multipart'] = array_merge($multipart['multipart'], [
                [
                    'name'     => 'project',
                    'contents' => $project,
                    'headers'  => [
                        'Content-Type' => 'application/json'
                    ]
                ]
            ]);
        }

        return $this->client->request('POST', 'markdown', $multipart);
    }
}
