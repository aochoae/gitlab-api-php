<?php
/**
 * Namespaces API
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab\Standalone;

use GitLab\AbstractResource;
use GitLab\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Namespaces API
 *
 * @link https://docs.gitlab.com/ee/api/version.html
 *
 * @since 1.0.0
 */
final class Namespaces extends AbstractResource
{
    /**
     * {@inheritDoc}
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
    }

    /**
     * Get a list of the namespaces of the authenticated user.
     *
     * GET /namespaces
     *
     * @link https://docs.gitlab.com/ee/api/namespaces.html#list-namespaces
     *
     * @since 1.0.0
     */
    public function getNamespaces(): ResponseInterface
    {
        return $this->client->request('GET', 'namespaces');
    }

    /**
     * Get all namespaces that match a string in their name or path.
     *
     * GET /namespaces?search=foobar
     *
     * @link https://docs.gitlab.com/ee/api/namespaces.html#search-for-namespace
     *
     * @since 1.0.0
     *
     * @param string $search Returns a list of namespaces the user is authorized to see based on the search criteria.
     */
    public function searchNamespace(string $search): ResponseInterface
    {
        return $this->client->request('GET', "namespaces?search=$search");
    }

    /**
     * Get a namespace by ID.
     *
     * GET /namespaces/:id
     *
     * @link https://docs.gitlab.com/ee/api/namespaces.html#get-namespace-by-id
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the namespace.
     */
    public function getNamespaceById($id): ResponseInterface
    {
        $namespaces_id = $this->getId($id);

        return $this->client->request('GET', "namespaces/$namespaces_id");
    }
}
