<?php
/**
 * Validate the .gitlab-ci.yml API
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab\Standalone;

use GitLab\AbstractResource;
use GitLab\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Validate the .gitlab-ci.yml API
 *
 * @link https://docs.gitlab.com/ee/api/lint.html
 *
 * @since 1.0.0
 */
final class Lint extends AbstractResource
{
    /**
     * {@inheritDoc}
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
    }

    /**
     * Checks if your .gitlab-ci.yml file is valid.
     *
     * POST /ci/lint
     *
     * @since 1.0.0
     *
     * @param string $content The .gitlab-ci.yaml content.
     */
    public function isValid(string $content): ResponseInterface
    {
        return $this->client->request('POST', 'ci/lint', [
            'multipart' => [
                [
                    'name'     => 'content',
                    'contents' => $content,
                    'headers'  => [
                        'Content-Type' => 'application/json'
                    ]
                ]
            ]
        ]);
    }
}
