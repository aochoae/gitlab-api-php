<?php
/**
 * Plan limits
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab\Standalone;

use GitLab\AbstractResource;
use GitLab\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Plan limits API
 *
 * @link https://docs.gitlab.com/ee/api/plan_limits.html
 *
 * @since 1.0.0
 */
final class PlanLimits extends AbstractResource
{
    /**
     * {@inheritDoc}
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
    }

    /**
     * List the current limits of a plan on the GitLab instance.
     *
     * GET /application/plan_limits
     *
     * @param string $plan_name Name of the plan to get the limits from. Default: default.
     */
    public function getCurrentPlanLimits(string $plan_name = 'default'): ResponseInterface
    {
        return $this->client->request('GET', 'application/plan_limits', [
            'query' => ['plan_name' => $plan_name]
        ]);
    }

    /**
     * Modify the limits of a plan on the GitLab instance.
     *
     * PUT /application/plan_limits
     *
     * @param string $plan_name Name of the plan to update.
     * @param array $attributes
     */
    public function changePlanLimits(string $plan_name = 'default', array $attributes = []): ResponseInterface
    {
        $mandatory = compact("plan_name");

        $query = $mandatory + $attributes;

        return $this->client->request('PUT', 'application/plan_limits', [
            'query' => $query
        ]);
    }
}
