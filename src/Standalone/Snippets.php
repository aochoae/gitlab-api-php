<?php
/**
 * Snippets API
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab\Standalone;

use GitLab\AbstractResource;
use GitLab\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Snippets API
 *
 * @link https://docs.gitlab.com/ee/api/snippets.html
 *
 * @since 1.0.0
 */
final class Snippets extends AbstractResource
{
    /**
     * {@inheritDoc}
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
    }

    /**
     * Get a list of the current user's snippets.
     *
     * GET /snippets
     *
     * @link https://docs.gitlab.com/ee/api/snippets.html#list-all-snippets-for-a-user
     *
     * @since 1.0.0
     */
    public function getSnippets(): ResponseInterface
    {
        return $this->client->request('GET', 'snippets');
    }

    /**
     * List all public snippets.
     *
     * GET /snippets/public
     *
     * @link https://docs.gitlab.com/ee/api/snippets.html#list-all-public-snippets
     *
     * @since 1.0.0
     *
     * @param array $attributes Parameters.
     */
    public function getPublic(array $attributes = []): ResponseInterface
    {
        return $this->client->request('GET', 'snippets/public', [
            'query' => $attributes
        ]);
    }

    /**
     * Get the information of a snippet.
     *
     * GET /snippets/:id
     *
     * @link https://docs.gitlab.com/ee/api/snippets.html#get-a-single-snippet
     *
     * @since 1.0.0
     *
     * @param int $snippet_id The ID of snippet to retrieve.
     */
    public function getSnippet(int $snippet_id): ResponseInterface
    {
        return $this->client->request('GET', "snippets/$snippet_id");
    }

    /**
     * Get a single snippet's raw contents.
     *
     * GET /snippets/:id/raw
     *
     * @link https://docs.gitlab.com/ee/api/snippets.html#single-snippet-contents
     *
     * @since 1.0.0
     *
     * @param int $snippet_id The ID of snippet to retrieve.
     */
    public function getContent(int $snippet_id): ResponseInterface
    {
        return $this->client->request('GET', "snippets/$snippet_id/raw");
    }

    /**
     * Creates a new snippet.
     *
     * POST /snippets
     *
     * @link https://docs.gitlab.com/ee/api/snippets.html#create-new-snippet
     *
     * @since 1.0.0
     *
     * @param string $title The title of the snippet.
     * @param string $file_name The file name of the snippet file.
     * @param string $content The content of the snippet.
     * @param string $visibility The snippet's visibility.
     * @param string $description The description of the snippet.
     */
    public function create(string $title, string $file_name, string $content, string $visibility = '', string $description = ''): ResponseInterface
    {
        $query = compact("title", "file_name", "content", "visibility", "description");

        return $this->client->request('POST', 'snippets', [
            'query' => $query
        ]);
    }

    /**
     * Updates an existing snippet.
     *
     * PUT /snippets/:id
     *
     * @link https://docs.gitlab.com/ee/api/snippets.html#update-snippet
     *
     * @since 1.0.0
     *
     * @param int $snippet_id The ID of snippet to update.
     * @param array $attributes Parameters.
     */
    public function update(int $snippet_id, array $attributes = []): ResponseInterface
    {
        return $this->client->request('PUT', "snippets/$snippet_id", [
            'query' => $attributes
        ]);
    }

    /**
     * Deletes an existing snippet.
     *
     * DELETE /snippets/:id
     *
     * @link https://docs.gitlab.com/ee/api/snippets.html#delete-snippet
     *
     * @since 1.0.0
     *
     * @param int $snippet_id The ID of snippet to delete.
     */
    public function delete(int $snippet_id): ResponseInterface
    {
        return $this->client->request('DELETE', "snippets/$snippet_id");
    }

    /**
     * Get user agent details.
     *
     * GET /snippets/:id/user_agent_detail
     *
     * @link https://docs.gitlab.com/ee/api/snippets.html#get-user-agent-details
     *
     * @since 1.0.0
     *
     * @param int $snippet_id The ID of snippet.
     */
    public function getUserAgent(int $snippet_id): ResponseInterface
    {
        return $this->client->request('GET', "snippets/$snippet_id/user_agent_detail");
    }
}
