<?php
/**
 * License API
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab\Standalone;

use GitLab\AbstractResource;
use GitLab\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * License API
 *
 * @link https://docs.gitlab.com/ee/api/license.html
 *
 * @since 1.0.0
 */
final class License extends AbstractResource
{
    /**
     * {@inheritDoc}
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
    }

    /**
     * Retrieve information about the current license.
     *
     * GET /license
     *
     * @link https://docs.gitlab.com/ee/api/license.html#retrieve-information-about-the-current-license
     *
     * @since 1.0.0
     */
    public function getLicense(): ResponseInterface
    {
        return $this->client->request('GET', 'license');
    }

    /**
     * Retrieve information about all licenses.
     *
     * GET /licenses
     *
     * @link https://docs.gitlab.com/ee/api/license.html#retrieve-information-about-all-licenses
     *
     * @since 1.0.0
     */
    public function getLicenses(): ResponseInterface
    {
        return $this->client->request('GET', 'licenses');
    }

    /**
     * Add a new license.
     *
     * POST /license
     *
     * @link https://docs.gitlab.com/ee/api/license.html#add-a-new-license
     *
     * @since 1.0.0
     *
     * @param string $license The license string.
     */
    public function add(string $license): ResponseInterface
    {
        return $this->client->request('POST', 'license', [
            'query' => ['license' => $license]
        ]);
    }

    /**
     * Delete a license.
     *
     * DELETE /license/:id
     *
     * @link https://docs.gitlab.com/ee/api/license.html#delete-a-license
     *
     * @since 1.0.0
     *
     * @param int $id The ID of the GitLab license.
     */
    public function delete(int $id): ResponseInterface
    {
        return $this->client->request('DELETE', "license/$id");
    }
}
