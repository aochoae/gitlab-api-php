<?php
declare(strict_types=1);

namespace GitLab\Test\Groups;

use PHPUnit\Framework\TestCase;

class GroupsTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    /**
     * vendor/bin/phpunit --filter testGroups tests/Groups/GroupsTest.php
     */
    public function testGroups()
    {
        $client = $this->getClient();

        $groups = new \GitLab\Groups\Groups($client);
        $response = $groups->getGroups();

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * vendor/bin/phpunit --filter testGetGroup tests/Groups/GroupsTest.php
     */
    public function testGetGroup()
    {
        $client = $this->getClient();

        $group_id = getenv('GITLAB_GROUP_ID');

        $groups = new \GitLab\Groups\Groups($client);
        $response = $groups->getGroup($group_id);

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * vendor/bin/phpunit --filter testGetProjects tests/Groups/GroupsTest.php
     */
    public function testGetProjects()
    {
        $client = $this->getClient();

        $group_id = getenv('GITLAB_GROUP_ID');

        $groups = new \GitLab\Groups\Groups($client);
        $response = $groups->getProjects($group_id);

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * vendor/bin/phpunit --filter testGetSubgroups tests/Groups/GroupsTest.php
     */
    public function testGetSubgroups()
    {
        $client = $this->getClient();

        $group_id = getenv('GITLAB_GROUP_ID');

        $groups = new \GitLab\Groups\Groups($client);
        $response = $groups->getSubgroups($group_id);

        $this->assertEquals(200, $response->getStatusCode());
    }
}
