<?php
declare(strict_types=1);

namespace GitLab\Test\Groups;

use PHPUnit\Framework\TestCase;

class MembersTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    /**
     * vendor/bin/phpunit --filter testGetMembers tests/Groups/MembersTest.php
     */
    public function testGetMembers()
    {
        $client = $this->getClient();

        $group_id = intval(getenv('GITLAB_GROUP_ID'));

        $groups = new \GitLab\Groups\Members($client);
        $response = $groups->getMembers($group_id);

        $this->assertEquals(200, $response->getStatusCode());
    }
}
