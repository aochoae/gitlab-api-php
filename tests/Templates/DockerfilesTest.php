<?php

namespace GitLab\Test\Templates;

use PHPUnit\Framework\TestCase;

class DockerfilesTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    /**
     * vendor/bin/phpunit --filter testGetListTemplates tests/Templates/DockerfilesTest.php
     */
    public function testGetListTemplates()
    {
        $client = $this->getClient();

        $dockerfiles = new \GitLab\Templates\Dockerfiles($client);
        $response = $dockerfiles->getListTemplates();

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * vendor/bin/phpunit --filter testGetSingleTemplate tests/Templates/DockerfilesTest.php
     */
    public function testGetSingleTemplate()
    {
        $client = $this->getClient();

        $dockerfiles = new \GitLab\Templates\Dockerfiles($client);
        $response = $dockerfiles->getSingleTemplate('Binary');

        $this->assertEquals(200, $response->getStatusCode());
    }
}
