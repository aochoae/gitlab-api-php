<?php
declare(strict_types=1);

namespace GitLab\Test\Projects;

use PHPUnit\Framework\TestCase;

class RepositoryFilesTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    /**
     * vendor/bin/phpunit --filter testGetFile tests/Projects/RepositoryFilesTest.php
     */
    public function testGetFile()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $tags = new \GitLab\Projects\RepositoryFiles($client);
        $response = $tags->getFile($project_id, 'README.md');

        $this->assertEquals(200, $response->getStatusCode());
    }
}
