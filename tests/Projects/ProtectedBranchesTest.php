<?php
declare(strict_types=1);

namespace GitLab\Test\Projects;

use PHPUnit\Framework\TestCase;

class ProtectedBranchesTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    /**
     * vendor/bin/phpunit --filter testGetBranches tests/Projects/ProtectedBranchesTest.php
     */
    public function testGetBranches()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $branches = new \GitLab\Projects\ProtectedBranches($client);
        $response = $branches->getBranches($project_id);

        $this->assertEquals(200, $response->getStatusCode());
    }
}
