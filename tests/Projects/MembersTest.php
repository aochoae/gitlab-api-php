<?php
declare(strict_types=1);

namespace GitLab\Test\Projects;

use PHPUnit\Framework\TestCase;

class MembersTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    /**
     * vendor/bin/phpunit --filter testGetMembers tests/Projects/MembersTest.php
     */
    public function testGetMembers()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $projects = new \GitLab\Projects\Members($client);
        $response = $projects->getMembers($project_id);

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * vendor/bin/phpunit --filter testGetAllMembers tests/Projects/MembersTest.php
     */
    public function testGetAllMembers()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $projects = new \GitLab\Projects\Members($client);
        $response = $projects->getAllMembers($project_id);

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * vendor/bin/phpunit --filter testGetMember tests/Projects/MembersTest.php
     */
    public function testGetMember()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');
        $user_id = getenv('GITLAB_USER_ID');

        $projects = new \GitLab\Projects\Members($client);
        $response = $projects->getMember($project_id, intval($user_id));

        $this->assertEquals(200, $response->getStatusCode());
    }
}
