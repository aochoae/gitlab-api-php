<?php
declare(strict_types=1);

namespace GitLab\Test\Projects;

use PHPUnit\Framework\TestCase;

class StatisticsTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    /**
     * vendor/bin/phpunit --filter testGetStatistics tests/Projects/StatisticsTest.php
     */
    public function testGetStatistics()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $tags = new \GitLab\Projects\Statistics($client);
        $response = $tags->getStatistics($project_id);

        $this->assertEquals(200, $response->getStatusCode());
    }
}
