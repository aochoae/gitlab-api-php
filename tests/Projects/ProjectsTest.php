<?php
declare(strict_types=1);

namespace GitLab\Test\Projects;

use PHPUnit\Framework\TestCase;

class ProjectsTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    /**
     * vendor/bin/phpunit --filter testGetProjects tests/Projects/ProjectsTest.php
     *
     * @return void
     */
    public function testGetProjects()
    {
        $client = $this->getClient();

        $projects = new \GitLab\Projects\Projects($client);
        $response = $projects->getProjects(['archived' => true]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * vendor/bin/phpunit --filter testGetUserProjects tests/Projects/ProjectsTest.php
     */
    public function testGetUserProjects()
    {
        $client = $this->getClient();

        $user = getenv('GITLAB_USER_ID');

        $projects = new \GitLab\Projects\Projects($client);
        $response = $projects->getUserProjects($user);

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * vendor/bin/phpunit --filter testGetProject tests/Projects/ProjectsTest.php
     */
    public function testGetProject()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $projects = new \GitLab\Projects\Projects($client);
        $response = $projects->getProject($project_id);
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * vendor/bin/phpunit --filter testGetProjectUsers tests/Projects/ProjectsTest.php
     */
    public function testGetProjectUsers()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $projects = new \GitLab\Projects\Projects($client);
        $response = $projects->getProjectUsers($project_id);
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * vendor/bin/phpunit --filter testGetForks tests/Projects/ProjectsTest.php
     */
    public function testGetForks()
    {
        $this->expectException(\GuzzleHttp\Exception\RequestException::class);

        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $projects = new \GitLab\Projects\Projects($client);
        $response = $projects->getForks($project_id);
    }

    /**
     * vendor/bin/phpunit --filter testStar tests/Projects/ProjectsTest.php
     * 
     * @group skip
     */
     public function testStar()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $projects = new \GitLab\Projects\Projects($client);
        $response = $projects->star($project_id);
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * vendor/bin/phpunit --filter testUnstar tests/Projects/ProjectsTest.php
     * 
     * @group skip
     */
    public function testUnstar()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $projects = new \GitLab\Projects\Projects($client);
        $response = $projects->unstar($project_id);
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * vendor/bin/phpunit --filter testGetLanguages tests/Projects/ProjectsTest.php
     */
    public function testGetLanguages()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $projects = new \GitLab\Projects\Projects($client);
        $response = $projects->getLanguages($project_id);

        $this->assertEquals(200, $response->getStatusCode());
    }
}
