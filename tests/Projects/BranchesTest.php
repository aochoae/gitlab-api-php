<?php
declare(strict_types=1);

namespace GitLab\Test\Projects;

use PHPUnit\Framework\TestCase;

class BranchesTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    /**
     * vendor/bin/phpunit --filter testGetBranches tests/Projects/BranchesTest.php
     */
    public function testGetBranches()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $branches = new \GitLab\Projects\Branches($client);
        $response = $branches->getBranches($project_id);

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * vendor/bin/phpunit --filter testGetBranch tests/Projects/BranchesTest.php
     */
    public function testGetBranch()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $branches = new \GitLab\Projects\Branches($client);
        $response = $branches->getBranch($project_id, 'main');

        $this->assertEquals(200, $response->getStatusCode());
    }
}
