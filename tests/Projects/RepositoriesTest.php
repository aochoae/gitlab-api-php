<?php
declare(strict_types=1);

namespace GitLab\Test\Projects;

use PHPUnit\Framework\TestCase;

class RepositoriesTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    /**
     * vendor/bin/phpunit --filter testGetTree tests/Projects/RepositoriesTest.php
     */
    public function testGetTree()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $projects = new \GitLab\Projects\Repositories($client);
        $response = $projects->getTree($project_id);
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * vendor/bin/phpunit --filter testGetBlob tests/Projects/RepositoriesTest.php
     */
    public function testGetBlob()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $projects = new \GitLab\Projects\Repositories($client);
        $response = $projects->getBlob($project_id, '9cfa63745746f4719006738dc9f9a7b1675855f9');
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * vendor/bin/phpunit --filter testGetFile tests/Projects/RepositoriesTest.php
     */
    public function testGetFile()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $projects = new \GitLab\Projects\Repositories($client);
        $response = $projects->getFile($project_id);
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * vendor/bin/phpunit --filter testGetContributors tests/Projects/RepositoriesTest.php
     */
    public function testGetContributors()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $projects = new \GitLab\Projects\Repositories($client);
        $response = $projects->getContributors($project_id);
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * TODO buscar valores hash
     * 
     * vendor/bin/phpunit --filter testCompare tests/Projects/RepositoriesTest.php
     * 
     * @group skip
     */
    public function testCompare()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');
        $commit = getenv('CI_COMMIT_SHA');
        $commit_before = getenv('CI_COMMIT_BEFORE_SHA');

        $projects = new \GitLab\Projects\Repositories($client);
        $response = $projects->compare($project_id, $commit_before, $commit);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
