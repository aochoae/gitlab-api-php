<?php
declare(strict_types=1);

namespace GitLab\Test;

use GitLab\AbstractResource;
use PHPUnit\Framework\TestCase;

class FunctionsTest extends TestCase
{
    public function testGetId() {

        $mock = $this->getMockBuilder(AbstractResource::class)
            ->onlyMethods(['getId'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->assertEquals(125, $mock->getId(125));
    }

    public function testGetIdUrl() {

        $mock = $this->getMockBuilder(AbstractResource::class)
            ->onlyMethods(['getId'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->assertEquals("project-url", $mock->getId("project-url"));
    }

    public function testGetIdNegative() {

        $mock = $this->getMockBuilder(AbstractResource::class)
            ->onlyMethods(['getId'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->expectException(\InvalidArgumentException::class);
        $this->assertEquals(-125, $mock->getId(-125));
    }

    public function testGetIdSanitized() {

        $mock = $this->getMockBuilder(AbstractResource::class)
            ->onlyMethods(['getId'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->assertEquals("project%26url", $mock->getId("project&url"));
    }
}
