<?php
declare(strict_types=1);

namespace GitLab\Test\Standalone;

use PHPUnit\Framework\TestCase;

class MarkdownTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    /**
     * vendor/bin/phpunit --filter testRender tests/Standalone/MarkdownTest.php
     */
    public function testRender()
    {
        $client = $this->getClient();

        $content = <<<'EOD'

# Markdown

EOD;

        $lint = new \GitLab\Standalone\Markdown($client);
        $response = $lint->render($content, true, 'aochoae/gitlab-api-php');

        $this->assertEquals(201, $response->getStatusCode());
    }
}
