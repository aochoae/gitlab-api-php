<?php
declare(strict_types=1);

namespace GitLab\Test\Standalone;

use PHPUnit\Framework\TestCase;

class AvatarTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    /**
     * vendor/bin/phpunit --filter testGetAvatar tests/Standalone/AvatarTest.php
     */
    public function testGetAvatar()
    {
        $client = $this->getClient();

        $email = getenv('GITLAB_USER_EMAIL');

        $version = new \GitLab\Standalone\Avatar($client);

        $response = $version->getAvatar($email, 50);

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * vendor/bin/phpunit --filter testGetAvatarEmailFail tests/Standalone/AvatarTest.php
     */
    public function testGetAvatarEmailFail()
    {
        $client = $this->getClient();

        $avatar = new \GitLab\Standalone\Avatar($client);

        $this->expectException(\InvalidArgumentException::class);
        $response = $avatar->getAvatar('lu#is@luis.c\'om', 50);
    }

    /**
     * vendor/bin/phpunit --filter testGetAvatarSizeFail tests/Standalone/AvatarTest.php
     */
    public function testGetAvatarSizeFail()
    {
        $client = $this->getClient();

        $avatar = new \GitLab\Standalone\Avatar($client);

        $this->expectException(\InvalidArgumentException::class);
        $response = $avatar->getAvatar('user@example.com', -50);
    }
}
