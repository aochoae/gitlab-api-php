<?php
declare(strict_types=1);

namespace GitLab\Test\Standalone;

use PHPUnit\Framework\TestCase;

class SnippetsTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    /**
     * vendor/bin/phpunit --filter testGetSnippets tests/Standalone/SnippetsTest.php
     */
    public function testGetSnippets()
    {
        $client = $this->getClient();

        $snippets = new \GitLab\Standalone\Snippets($client);
        $response = $snippets->getSnippets();

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * vendor/bin/phpunit --filter testGetPublic tests/Standalone/SnippetsTest.php
     */
    public function testGetPublic()
    {
        $client = $this->getClient();

        $snippets = new \GitLab\Standalone\Snippets($client);
        $response = $snippets->getPublic(['per_page' => 5, 'page' => 1]);

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * vendor/bin/phpunit --filter testGetSnippet tests/Standalone/SnippetsTest.php
     */
    public function testGetSnippet()
    {
        $client = $this->getClient();

        $snippet_id = intval(getenv('GITLAB_SNIPPET_ID'));

        $snippets = new \GitLab\Standalone\Snippets($client);
        $response = $snippets->getSnippet($snippet_id);

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * vendor/bin/phpunit --filter testGetContent tests/Standalone/SnippetsTest.php
     */
    public function testGetContent()
    {
        $client = $this->getClient();

        $snippet_id = intval(getenv('GITLAB_SNIPPET_ID'));

        $snippets = new \GitLab\Standalone\Snippets($client);
        $response = $snippets->getContent($snippet_id);

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * vendor/bin/phpunit --filter testGetUserAgent tests/Standalone/SnippetsTest.php
     */
    public function testGetUserAgent()
    {
        $this->expectException(\GuzzleHttp\Exception\RequestException::class);

        $client = $this->getClient();

        $snippet_id = intval(getenv('GITLAB_SNIPPET_ID'));

        $projects = new \GitLab\Standalone\Snippets($client);
        $response = $projects->getUserAgent($snippet_id);
    }
}
