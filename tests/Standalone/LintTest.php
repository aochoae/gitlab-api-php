<?php
declare(strict_types=1);

namespace GitLab\Test\Standalone;

use PHPUnit\Framework\TestCase;

class LintTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    /**
     * vendor/bin/phpunit --filter testLint tests/Standalone/LintTest.php
     */
    public function testLint()
    {
        $client = $this->getClient();

        $content = <<<'EOD'

# Official docker image.
image: docker:latest

services:
  - docker:dind

EOD;

        $lint = new \GitLab\Standalone\Lint($client);
        $response = $lint->isValid($content);

        $this->assertEquals(200, $response->getStatusCode());
    }
}
