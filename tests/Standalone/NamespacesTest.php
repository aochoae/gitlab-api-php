<?php
declare(strict_types=1);

namespace GitLab\Test\Standalone;

use PHPUnit\Framework\TestCase;

class NamespacesTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    /**
     * vendor/bin/phpunit --filter testGetNamespaces tests/Standalone/NamespacesTest.php
     */
    public function testGetNamespaces()
    {
        $client = $this->getClient();

        $namespaces = new \GitLab\Standalone\Namespaces($client);
        $response = $namespaces->getNamespaces();

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * vendor/bin/phpunit --filter testSearchNamespace tests/Standalone/NamespacesTest.php
     */
    public function testSearchNamespace()
    {
        $client = $this->getClient();

        $user = getenv('GITLAB_USER_NAME');

        $namespaces = new \GitLab\Standalone\Namespaces($client);
        $response = $namespaces->searchNamespace($user);

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * vendor/bin/phpunit --filter testGetNamespaceById tests/Standalone/NamespacesTest.php
     */
    public function testGetNamespaceById()
    {
        $client = $this->getClient();

        $namespace = getenv('CI_PROJECT_NAMESPACE');

        $namespaces = new \GitLab\Standalone\Namespaces($client);
        $response = $namespaces->getNamespaceById($namespace);

        $this->assertEquals(200, $response->getStatusCode());
    }
}
