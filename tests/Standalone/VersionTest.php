<?php
declare(strict_types=1);

namespace GitLab\Test\Standalone;

use PHPUnit\Framework\TestCase;

class VersionTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    /**
     * vendor/bin/phpunit --filter testVersion tests/Standalone/VersionTest.php
     */
    public function testVersion()
    {
        $client = $this->getClient();

        $version = new \GitLab\Standalone\Version($client);
        $response = $version->getVersion();

        $this->assertEquals(200, $response->getStatusCode());
    }
}
