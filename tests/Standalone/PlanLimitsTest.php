<?php
declare(strict_types=1);

namespace GitLab\Test\Standalone;

use PHPUnit\Framework\TestCase;

class PlanLimitsTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    /**
     * vendor/bin/phpunit --filter testGetCurrentPlanLimits tests/Standalone/PlanLimitsTest.php
     *
     * @group saas
     *
     * @return void
     */
    public function testGetCurrentPlanLimitsSaaS()
    {
        $this->expectException(\GuzzleHttp\Exception\RequestException::class);

        $client = $this->getClient();

        $planLimits = new \GitLab\Standalone\PlanLimits($client);
        $response = $planLimits->getCurrentPlanLimits();
    }
}
