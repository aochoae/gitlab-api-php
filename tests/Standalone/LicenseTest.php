<?php
declare(strict_types=1);

namespace GitLab\Test\Standalone;

use PHPUnit\Framework\TestCase;

class LicenseTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    /**
     * vendor/bin/phpunit --filter testGetLicense tests/Standalone/LicenseTest.php
     */
    public function testGetLicense()
    {
        $this->expectException(\GuzzleHttp\Exception\RequestException::class);

        $client = $this->getClient();

        $license = new \GitLab\Standalone\License($client);
        $response = $license->getLicense();
    }

    /**
     * vendor/bin/phpunit --filter testGetLicenses tests/Standalone/LicenseTest.php
     */
    public function testGetLicenses()
    {
        $this->expectException(\GuzzleHttp\Exception\RequestException::class);

        $client = $this->getClient();

        $license = new \GitLab\Standalone\License($client);
        $response = $license->getLicenses();
    }
}
