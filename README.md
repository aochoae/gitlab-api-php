# GitLab API

Automate GitLab via a simple and powerful API.

## Requirements

* GitLab version 14.7 or greater
* PHP version 7.4 or greater

## Install

    composer require luisalberto/gitlab:dev-main

## Usage

    <?php

    require_once(dirname( __FILE__ ) . '/vendor/autoload.php');

    $client = new \GitLab\Client([
        'debug'   => true,
        'headers' => [
            'PRIVATE-TOKEN' => 'your-private-token'
        ]
    ]);

    $projects = new \GitLab\Projects\Projects($client);
    $response = $projects->getProject(12345678);

    echo $response->getBody()->getContents();

## License

See [LICENSE](LICENSE) for the full license text.
